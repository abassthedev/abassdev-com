import Nav from './Nav/Nav'
import Profile from './Profile/Profile'
import Skill from './Skill/Skill'
import Project from './Project/Project'
import Services from './Services/Services'
import Footer from './Footer/Footer'
import LatestPosts from './Blog/LatestPosts'
import Shortcodes from './Blog/Shortcodes/Shortcodes'
import MyStory from './MyStory/MyStory'
import FAQ from './FAQ/FAQ'
import Home from './Home/Home'

export { Home, MyStory, LatestPosts, Nav, Profile, Skill, Project, Services, Shortcodes, Footer, FAQ }
